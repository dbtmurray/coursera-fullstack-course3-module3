class BikeResult < LegResult

  include Mongoid::Document

  field :mph, type: Float

  def calc_ave
    return nil if event.nil? || secs.nil?
    self.mph = (event.miles * 3600 / secs)
  end

end
