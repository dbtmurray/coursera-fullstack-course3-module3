class SwimResult < LegResult

  include Mongoid::Document

  field :pace_100, type: Float

  def calc_ave
    return nil if event.nil? || secs.nil?
    self.pace_100 = secs / (event.meters / 100)
  end

end
