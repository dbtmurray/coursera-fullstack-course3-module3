class RunResult < LegResult

  include Mongoid::Document

  field :mmile, as: :minute_mile, type: Float

  def calc_ave
    return nil if event.nil? || secs.nil?
    self.mmile = secs / (event.miles * 60)
  end

end
