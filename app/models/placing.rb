class Placing
  include Mongoid::Document

  attr_accessor :name, :place

  field :name, type: String
  field :place, type: Integer

  def mongoize
    {:name => @name, :place => @place }
  end

  def self.mongoize object
    case object
    when nil then nil
    when Hash then object
    when Placing then object.mongoize
    end
  end

  def self.evolve object
    Placing.mongoize object
  end

  def self.demongoize object
    case object
    when nil then nil
    when Hash then Placing.new(object)
    when Placing then objet
    end
  end

  def initialize(hash)
    @name = hash[:name]
    @place = hash[:place]
  end


end
