class Address
  include Mongoid::Document

  attr_accessor :city, :state, :location
  field :city, type: String
  field :state, type: String
  field :location, type: Point

  def mongoize
    {:city => "#{@city}", :state => "#{@state}", :loc => Point.mongoize(@location) }
  end

  def self.mongoize object
    case object
    when nil then nil
    when Hash then object
    when Address then object.mongoize
    end
  end

  def self.evolve object
    Address.mongoize object
  end

  def self.demongoize object
    case object
    when nil then nil
    when Hash then Address.new(object)
    when Address then object
    end
  end

  def initialize(hash={})
    @city = hash[:city]
    @state = hash[:state]
    @location = Point.new(hash[:loc])
  end


end
