class Point

  include Mongoid::Document

  attr_accessor :longitude, :latitude
  field :longitude, type: Float
  field :latitude, type: Float

  def mongoize
    {:type => "Point", :coordinates => [@longitude, @latitude]}
  end

  def self.mongoize object
    case object
    when nil then nil
    when Hash then object
    when Point then object.mongoize
    end
  end

  def self.evolve object
    Point.mongoize object
  end

  def self.demongoize object
    case object
    when nil then nil
    when Hash then Point.new(object)
    when Point then object
    end
  end

  def initialize(hash={})
    return if hash.nil? || hash[:coordinates].nil?
    @longitude = hash[:coordinates][0]
    @latitude = hash[:coordinates][1]
  end

end
