class Race
  include Mongoid::Document
  include Mongoid::Timestamps

  field :n, as: :name, type: String
  field :date, type: Date
  field :loc, as: :location, type: Address
  field :next_bib, type: Integer

  embeds_many :events, as: :parent, order: [:order.asc]
  has_many :entrants, foreign_key: "race._id", dependent: :delete, order: [:secs.asc, :bib.asc]
  
  scope :upcoming, ->{where(:date.gte => Date.current)}
  scope :past, ->{where(:date.lt => Date.current)}

  # this should be a different callback, I think
  # but hacked it by only initializing if currently nil
  after_initialize :init_next_bib

  DEFAULT_EVENTS = {"swim" => {:order => 0, :name => "swim", :distance => 1.0, :units => "miles"},
                    "t1" => {:order => 1, :name => "t1"},
                    "bike" => {:order => 2, :name => "bike", :distance => 25.0, :units => "miles"},
                    "t2" => {:order => 3, :name => "t2"},
                    "run" => {:order => 4, :name => "run", :distance => 10.0, :units => "kilometers"}}

  def init_next_bib
    self[:next_bib] = 0 if self[:next_bib].nil?
  end


  def swim
    event = events.select { |event| "swim" == event.name }.first
    event ||= events.build(DEFAULT_EVENTS["swim"])
  end

  def swim_order
    swim.order
  end

  def swim_order= o
    swim.order = o
  end

  def swim_distance
    swim.distance
  end

  def swim_distance= d
    swim.distance = d
  end

  def swim_units
    swim.units
  end

  def swim_units= u
    swim.units = u
  end

  def bike
    event = events.select { |event| "bike" == event.name }.first
    event ||= events.build(DEFAULT_EVENTS["bike"])
  end

  def bike_order
    bike.order
  end

  def bike_order= o
    bike.order = o
  end

  def bike_distance
    bike.distance
  end

  def bike_distance= d
    bike.distance = d
  end

  def bike_units
    bike.units
  end

  def bike_units= u
    bike.units = u
  end

  def run
    event = events.select { |event| "run" == event.name }.first
    event ||= events.build(DEFAULT_EVENTS["run"])
  end

  def run_order
    run.order
  end

  def run_order= o
    run.order = o
  end

  def run_distance
    run.distance
  end

  def run_distance= d
    run.distance = d
  end

  def run_units
    run.units
  end
  
  def run_units= u
    run.units = u
  end

  def t1
    event = events.select { |event| "t1" == event.name }.first
    event ||= events.build(DEFAULT_EVENTS["t1"])
  end

  def t1_order
    t1.order
  end

  def t1_order= o
    t1.order = o
  end

  def t2
    event = events.select { |event| "t2" == event.name }.first
    event ||= events.build(DEFAULT_EVENTS["t2"])
  end

  def t2_order
    t2.order
  end

  def t2_order= o
    t2.order = o
  end

  def self.default
    Race.new do |race|
      DEFAULT_EVENTS.keys.each { |leg| race.send("#{leg}") }
    end
  end

  def city
    self.location.nil? ? nil : self.location.city
  end

  def city= c
    loc = self.location ||= Address.new
    loc.city = c
    self.location = loc
  end

  def state
    self.location.nil? ? nil : self.location.state
  end

  def state= s
    loc = self.location ||= Address.new
    loc.state = s
    self.location = loc
  end
  
  def next_bib
    self.inc(next_bib: 1)
    self[:next_bib]
  end

  def get_group racer
    if racer && racer.birth_year && racer.gender
      quotient = (date.year - racer.birth_year) / 10
      min_age = quotient * 10
      max_age = min_age + 9
      gender = racer.gender
      name = min_age >= 60 ? "masters #{gender}" : "#{min_age} to #{max_age} (#{gender})"
      Placing.demongoize(:name => name)
    end
  end

  def create_entrant racer
    entrant = Entrant.new
    entrant.race[:_id] = self[:_id]
    entrant.race.name = name
    entrant.race.date = date
    entrant.racer.first_name = racer.first_name
    entrant.racer.last_name = racer.last_name
    entrant.racer.gender = racer.gender
    entrant.racer.birth_year = racer.birth_year
    entrant.racer.city = racer.city
    entrant.racer.state = racer.state
    entrant.racer.racer_id = racer[:_id]
    entrant.group = get_group racer
    events.each do |event|
      entrant.send("#{event.name}=", event)
    end
    if entrant.validate
      entrant.bib = next_bib
      entrant.save
      entrant.racer.save
      entrant.race.save
    end
    entrant
  end

  def self.upcoming_available_to racer
    registered = Racer.find(racer.id).races.upcoming.pluck(:race).map { |r| r[:_id] } # why does racer.races not work??
    Race.upcoming.where(:_id.nin => registered)
  end

end
