class Entrant
  include Mongoid::Document
  include Mongoid::Timestamps

  store_in collection: :results
  embeds_many :results, class_name: "LegResult", order: [:"event.o".asc], after_add: :update_total, after_remove: :update_total
  embeds_one :race, class_name: "RaceRef", autobuild: true
  embeds_one :racer, as: :parent, class_name: "RacerInfo", autobuild: true

  field :bib, type: Integer
  field :secs, type: Float
  field :o, as: :overall, type: Placing
  field :gender, type: Placing
  field :group, type: Placing

  delegate :first_name, :first_name=, to: :racer
  delegate :last_name, :last_name=, to: :racer
  delegate :gender, :gender=, to: :racer, prefix: "racer"
  delegate :birth_year, :birth_year=, to: :racer
  delegate :city, :city=, to: :racer
  delegate :state, :state=, to: :racer

  delegate :name, :name=, to: :race, prefix: "race"
  delegate :date, :date=, to: :race, prefix: "race"

  scope :upcoming, ->{where(:"race.date".gte => Date.current)}
  scope :past, ->{where(:"race.date".lt => Date.current)}

  def update_total(result)
    secs = 0
    results.each { |r| secs += (r.secs || 0) }
    self.secs = secs
  end

  def the_race
    race.race
  end

  def overall_place
    overall.place if overall
  end

  def gender_place
    gender.place if gender
  end

  def group_name
    group.name if group
  end

  def group_place
    group.place if group
  end

  def swim
    result = results.select { |r| "swim" == r.event.name if r.event }.first
    if !result
      result = SwimResult.new(:event => {:name => "swim"})
      results << result
    end
    result
  end

  def swim= event
    self.swim.build_event(event.attributes)
    update_total nil
  end

  def run
    result = results.select { |r| "run" == r.event.name if r.event }.first
    if !result
      result = RunResult.new(:event => {:name => "run"})
      results << result
    end
    result
  end

  def run= event
    self.run.build_event(event.attributes)
    update_total nil
  end

  def bike
    result = results.select { |r| "bike" == r.event.name if r.event }.first
    if !result
      result = BikeResult.new(:event => {:name => "bike"})
      results << result
    end
    result
  end

  def bike= event
    self.bike.build_event(event.attributes)
    update_total nil
  end

  def t1
    result = results.select { |r| "t1" == r.event.name if r.event }.first
    if !result
      result = LegResult.new(:event => {:name => "t1"})
      results << result
    end
    result
  end

  def t1= event
    self.t1.build_event(event.attributes)
    update_total nil
  end

  def t2
    result = results.select { |r| "t2" == r.event.name if r.event }.first
    if !result
      result = LegResult.new(:event => {:name => "t2"})
      results << result
    end
    result
  end

  def t2= event
    self.t2.build_event(event.attributes)
    update_total nil
  end

  def swim_secs
    swim.secs
  end

  def swim_secs= s
    swim.secs = s
    update_total nil
  end

  def swim_pace_100
    swim.pace_100
  end

  def swim_pace_100= p
    swim.pace_100 = p
    update_total nil
  end

  def t1_secs
    t1.secs
  end

  def t1_secs= s
    t1.secs= s
    update_total nil
  end

  def bike_secs
    bike.secs
  end

  def bike_secs= s
    bike.secs = s
    update_total nil
  end
  
  def bike_mph
    bike.mph
  end

  def bike_mph= m
    bike.mph= m
    update_total nil
  end
  
  def t2_secs
    t2.secs
  end

  def t2_secs= s
    t2.secs= s
    update_total nil
  end

  def run_secs
    run.secs
  end

  def run_secs= s
    run.secs= s
    update_total nil
  end

  def run_mmile
    run.mmile
  end

  def run_mmile= m
    run.mmile = m
    update_total nil
  end



end
