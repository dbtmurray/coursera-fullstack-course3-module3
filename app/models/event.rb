class Event

  #attr_accessor :order, :name, :distance, :units

  include Mongoid::Document
  field :o, as: :order, type: Integer
  field :n, as: :name, type: String
  field :d, as: :distance, type: Float
  field :u, as: :units, type: String

  embedded_in :parent, polymorphic: true, touch: true

  validates_presence_of :order, :name

  def meters
    return nil if distance.nil?
    case units
    when "meters" then distance
    when "kilometers" then 1000 * distance
    when "yards" then 0.9144 * distance
    when "miles" then 1609.344 * distance
    end
  end

  def miles
    return nil if distance.nil? || meters.nil?
    meters * 0.000621371
  end

end
