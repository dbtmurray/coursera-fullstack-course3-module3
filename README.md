solution for this course https://www.coursera.org/learn/ruby-on-rails-web-services-mongodb/home/week/3

all the tests pass on my machine, but the grader fails. Shared here for anyone interested in
diagnosing the issue. See discussion at

https://www.coursera.org/learn/ruby-on-rails-web-services-mongodb/programming/XFOOW/mongoid-and-rails-scaffold/discussions/threads/bCdkYrW8Eei7DQrsAYGsmg
